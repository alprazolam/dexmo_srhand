#!/usr/bin/env python

import rospy
import socket

from std_msgs.msg import String

def server(sock):
    pub = rospy.Publisher('pose', String, queue_size=10)
    rospy.init_node('server', anonymous=True)
    rate = rospy.Rate(300) # Frequency

    server_address = ('', 10000)
    print "Server Address: ", server_address
    sock.bind(server_address)
    sock.listen(1)
    conn, addr = sock.accept()
    print "Connected: ", addr
    

    while not rospy.is_shutdown():
        data, address = conn.recvfrom(1024) # 2 time the 29 bytes long pose string (including <EOF>)
        # print "Message from ", address
        # rospy.loginfo(address)
        # print data
        head = data.find("<HEAD>")
        cut = data.find("<EOF>")
        # print "Head: " + str(head) + " Cut: " + str(cut) + " /n"
        if head >= 0 and cut > 0:
            pose = data[head + 7: cut - 1]
            print pose
            # print pose
            # pose = data.split(" ")
            # rospy.loginfo(pose)
            pub.publish(pose)
        rate.sleep()

if __name__ == '__main__':
    try:
        sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        server(sock)
    except rospy.ROSInterruptException:
        sock.close()
        pass
