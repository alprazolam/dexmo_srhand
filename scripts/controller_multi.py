#!/usr/bin/env python

import rospy
from sr_robot_commander.sr_hand_commander import SrHandCommander
from sr_utilities.hand_finder import HandFinder

import socket
import sys
import time
import threading


# FF, FF_spread, MF, MF_spread, RF, RF_spread, LF, LF_spread, TH, TH_spread, TH_mcp
POSE = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

# Thread lock
lock = threading.Lock()

# Thread switch
running = True


# The loop for robot controlling, running at (1 ** 6 / interval) Hz 
def control_loop(lock):
    # interval = 1000
    # last_tick = time.clock_gettime_ns(CLOCK_MONOTONIC)
    while running:
        # next_tick = last_tick + interval
        # time.sleep(next_tick - time.clock_gettime_ns(CLOCK_MONOTONIC))
        # last_tick = next_tick
        # TODO: check if the loop can run 1000 Hz
        # TODO: measure the comm_loop frequency
        with lock:
            #print "Control with lock"
            joints_states = {'rh_FFJ1': POSE[0], 'rh_FFJ2': POSE[0], 'rh_FFJ3': POSE[0], 'rh_FFJ4': POSE[1],
                            'rh_MFJ1': POSE[2], 'rh_MFJ2': POSE[2], 'rh_MFJ3': POSE[2], 'rh_MFJ4': POSE[3],
                            'rh_RFJ1': POSE[4], 'rh_RFJ2': POSE[4], 'rh_RFJ3': POSE[4], 'rh_RFJ4': POSE[5],
                            'rh_LFJ1': POSE[6], 'rh_LFJ2': POSE[6], 'rh_LFJ3': POSE[6], 'rh_LFJ4': POSE[7], 'rh_LFJ5': 0.0,
                            'rh_THJ1': POSE[8], 'rh_THJ2': POSE[8], 'rh_THJ3': POSE[9], 'rh_THJ4': POSE[10], 'rh_THJ5': 15,
                            'rh_WRJ1': 0.0, 'rh_WRJ2': 0.0}
        hand_commander.move_to_joint_value_target_unsafe(joints_states, wait=False, angle_degrees=True)

    return

# The loop for receiving (posting) data from dexmo server
def comm_loop(lock, sock):
    
    sock.listen(1)
    conn, addr = sock.accept()
    print "Connected: ", addr
    while running:
        data, address = sock.recvfrom(20)
        with lock:
            print "Comm with lock"
            POSE = [0, -10, 0, 10, 60, 0, 60, 0, 40, 65, 15]

    sock.close()
    sock.shutdown()
    return


rospy.init_node("shadow_publisher", anonymous=True)

hand_commander = SrHandCommander(name="right_hand")
print "Robot name: ", hand_commander.get_robot_name()
print "Group name: ", hand_commander.get_group_name()
print "Planning frame: ", hand_commander.get_planning_frame()

sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
server_address = ("::", 10000)
print "Server Address: ", server_address
sock.bind(server_address)

comm = threading.Thread(target=comm_loop, args=(lock, sock,))
control = threading.Thread(target=control_loop, args=(lock,))

comm.setDaemon(True)
control.setDaemon(True)

comm.start()
control.start()

rospy.spinonce()

input("Press Enter to continue...")

running = False

