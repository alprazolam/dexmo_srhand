#!/usr/bin/env python

import rospy
import socket
import sys

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = ('localhost', 10000)
message = 'This is the message.'

try:

    # Send data
    print "Sending: ", message
    sent = sock.sendto(message, server_address)

finally:
    print 'closing socket'
    sock.close()