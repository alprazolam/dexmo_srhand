#!/usr/bin/env python

import rospy
from sr_robot_commander.sr_hand_commander import SrHandCommander
from sr_utilities.hand_finder import HandFinder

from std_msgs.msg import String

def callback(data, args):
    hand_commander = args
    data = str(data)
    data = data.replace('"', '')
    POSE = data.split(" ")[1:]
    # print POSE
    # POSE = [0, -10, 0, 10, 60, 0, 60, 0, 40, 65, 15]
    joints_states = {'rh_FFJ1': float(POSE[0]), 'rh_FFJ2': float(POSE[0]), 'rh_FFJ3': float(POSE[0]), 'rh_FFJ4': float(POSE[1]),
                        'rh_MFJ1': float(POSE[2]), 'rh_MFJ2': float(POSE[2]), 'rh_MFJ3': float(POSE[2]), 'rh_MFJ4': float(POSE[3]),
                        'rh_RFJ1': float(POSE[4]), 'rh_RFJ2': float(POSE[4]), 'rh_RFJ3': float(POSE[4]), 'rh_RFJ4': float(POSE[5]),
                        'rh_LFJ1': float(POSE[6]), 'rh_LFJ2': float(POSE[6]), 'rh_LFJ3': float(POSE[6]), 'rh_LFJ4': float(POSE[7]), 'rh_LFJ5': 0.0,
                        'rh_THJ1': float(POSE[8]), 'rh_THJ2': float(POSE[8]), 'rh_THJ3': float(POSE[9]), 'rh_THJ4': float(POSE[10]), 'rh_THJ5': 15,
                        'rh_WRJ1': 0.0, 'rh_WRJ2': 0.0}
    hand_commander.move_to_joint_value_target_unsafe(joints_states, wait=False, angle_degrees=True)

def controller(hand_commander):
    rospy.Subscriber("pose", String, callback, (hand_commander))
    rospy.spin()

if __name__ == '__main__':
    rospy.init_node('controller', anonymous=True)
    hand_commander = SrHandCommander(name="right_hand")
    print "Robot name: ", hand_commander.get_robot_name()
    print "Group name: ", hand_commander.get_group_name()
    print "Planning frame: ", hand_commander.get_planning_frame()

    # POSE = [0, -10, 0, 10, 60, 0, 60, 0, 40, 65, 15]
    # joints_states = {'rh_FFJ1': float(POSE[0]), 'rh_FFJ2': float(POSE[0]), 'rh_FFJ3': float(POSE[0]), 'rh_FFJ4': float(POSE[1]),
    #                     'rh_MFJ1': float(POSE[2]), 'rh_MFJ2': float(POSE[2]), 'rh_MFJ3': float(POSE[2]), 'rh_MFJ4': float(POSE[3]),
    #                     'rh_RFJ1': float(POSE[4]), 'rh_RFJ2': float(POSE[4]), 'rh_RFJ3': float(POSE[4]), 'rh_RFJ4': float(POSE[5]),
    #                     'rh_LFJ1': float(POSE[6]), 'rh_LFJ2': float(POSE[6]), 'rh_LFJ3': float(POSE[6]), 'rh_LFJ4': float(POSE[7]), 'rh_LFJ5': 0.0,
    #                     'rh_THJ1': float(POSE[8]), 'rh_THJ2': float(POSE[8]), 'rh_THJ3': float(POSE[9]), 'rh_THJ4': float(POSE[10]), 'rh_THJ5': 15,
    #                     'rh_WRJ1': 0.0, 'rh_WRJ2': 0.0}
    # hand_commander.move_to_joint_value_target_unsafe(joints_states, wait=False, angle_degrees=True)

    controller(hand_commander)
